// ==UserScript==
// @name        自動延長無しとかそんなん確認しとらんよ…
// @namespace   http://ir9.jp/auction/auto_encho
// @description 自動延長無しのオークションを強調する
// @author      第２プログラマ いろきゅう <http://ir9.jp/>
// @grant       none
// @include     http://page*.auctions.yahoo.co.jp/jp/auction/*
// @version     3
// ==/UserScript==

function log(msg)
{
	console.log(msg);
}

function insertBlinkStyle()
{
	// log(10);
	var style = document.createElement('style');
	style.type = "text/css";
	style.innerText = "@keyframes auto_extension_blink_animation{\n" +
	" 0% { visibility : visible; } \n" +
	" 80%{ visibility : hidden;  } \n" +
	"}\n" + 
	"\n" + 
	".auto_extension_blink { \n" +
	" font-weight      : bold;\n" + 
	" color            : #fff;\n" + 
	" background-color : #c00;\n" + 
	" animation        : auto_extension_blink_animation 1s steps(1) infinite;\n" +
	"}";
	document.getElementsByTagName('head')[0].appendChild(style);
	// log(11);
}

function extendV2()
{
	function isElement(elem)
	{
		return elem.nodeType == 1;
	}

	function findOfHasTextNode(nodeList, text)
	{
		var len = nodeList.length;
		for(var i = 0; i < len; ++i)
		{
			var elem      = nodeList[i];
			var nodeValue = elem.innerHTML;
			if(nodeValue && (nodeValue.indexOf(text) >= 0))
				return elem;
		}
		return null;
	}

	function isAutoEnchoAuction(enchoTR)
	{
		var noText   = "なし";
		var children = enchoTR.childNodes;

		return findOfHasTextNode(children, noText) != null;
	}

	function getAutoEncho()
	{
		var autoText   = "自動延長";
		var modPdtInfo = document.getElementById('modPdtInfo');
		var ths        = modPdtInfo.getElementsByTagName("th");

		return findOfHasTextNode(ths, autoText);
	}

	function setWarningStyleForTR(tr)
	{
		tr.className += " auto_extension_blink";
	}

	function setWarningStyleForText(tr)
	{
		var children = tr.childNodes;
		var len      = children.length;
		for(var i = 0; i < len; ++i)
		{
			var elem = children[i];
			if(isElement(elem))
			{
				var style   = elem.style;
				style.color = "#fff";
			}
		}
	}

	var encho = getAutoEncho();
	// GM_log(encho.innerHTML);
	if(encho == null)
		return false;
	var enchoTR = encho.parentNode;
	if(!isAutoEnchoAuction(enchoTR))
		return false;

	// <tr><td>自動延長</td><td></td><td>あり/なし</td></tr>
	insertBlinkStyle();
	setWarningStyleForTR(enchoTR);
	setWarningStyleForText(enchoTR);
	
	return true;
}

// <li class="ProductDetail__item"><dl><dt><a>自動延長</a></dt><dd class="ProductDetail__description"><span>：</span>あり/なし</dd></li>
function extendV3()
{
	// 自動延長 を保持している <li> を取得
	function getAutoEncho()
	{
		var itemList = document.getElementsByClassName('ProductDetail__item');
		for(var i = 0; i < itemList.length; ++i)
		{
			var item  = itemList[i];
			var aList = item.getElementsByTagName("a");
			if(aList == null || aList.length <= 0)
				continue;
			var a = aList[0];
			if(a.innerText == "自動延長")
				return item;
		}
		return null;
	}

	// 自動延長が"なし"？
	function isAutoEnchoAuction(li)
	{
		var itemList = li.getElementsByClassName('ProductDetail__description');
		// log("2 : " + itemList.length);
		if(itemList == null || itemList <= 0)
			return false;
		var item = itemList[0];
		// log("1 : " + item.innerText);
		return item.innerText.indexOf("：なし") >= 0;
	}

	// blink を挿入
	function insertBlink(li)
	{
		// log(6);
		insertBlinkStyle();
		// log(7);
		li.className += " auto_extension_blink";
		// log(8);
	}

	var li = getAutoEncho();
	// log("5 : " + li);
	if(li == null)
		return false;
	// log(4);
	if(!isAutoEnchoAuction(li))
		return false;
	// log("3 : " + li.innerText);
	insertBlink(li);
	return true;
}

function main()
{
	if(extendV3())
		return; // おわり
	extendV2();
}

try {
	main();
} catch(e) {
	log(e);
}





